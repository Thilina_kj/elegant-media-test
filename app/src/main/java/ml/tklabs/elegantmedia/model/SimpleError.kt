package ml.tklabs.elegantmedia.model

class SimpleError {
    var message : String? = null
    var status : Int = 400
}