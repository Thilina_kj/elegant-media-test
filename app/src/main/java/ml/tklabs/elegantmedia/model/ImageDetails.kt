package ml.tklabs.elegantmedia.model

class ImageDetails {
    var small : String? = null
    var medium : String? = null
    var large : String? = null
}