package ml.tklabs.elegantmedia.model

class HotelListResponse {
    var status : Int = 400
    var data : List<Hotel>? = null
}