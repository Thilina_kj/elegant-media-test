package ml.tklabs.elegantmedia.model

object ResponseStates {
    const val SUCCESS = 200
    const val FAILURE = 400
    const val NO_INTERNET = 450
}