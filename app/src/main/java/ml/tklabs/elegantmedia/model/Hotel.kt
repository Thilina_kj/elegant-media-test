package ml.tklabs.elegantmedia.model

class Hotel {

    var id : Int = 0
    var title : String? = null
    var description : String? = null
    var address : String? = null
    var postcode : String? = null
    var phoneNumber : String? = null
    var latitude : String? = null
    var longitude : String? = null
    var image : ImageDetails? = null

}