package ml.tklabs.elegantmedia.model

import androidx.recyclerview.widget.DiffUtil

object DiffCallbacks {
    val Hotel: DiffUtil.ItemCallback<Hotel> = object : DiffUtil.ItemCallback<Hotel>() {
        override fun areItemsTheSame(oldItem: Hotel, newItem: Hotel): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Hotel, newItem: Hotel): Boolean {
            return oldItem.phoneNumber == newItem.phoneNumber &&
                    oldItem.postcode == newItem.postcode &&
                    oldItem.latitude == newItem.latitude &&
                    oldItem.longitude == newItem.longitude
        }
    }
}