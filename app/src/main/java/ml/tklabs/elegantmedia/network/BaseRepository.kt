package ml.tklabs.elegantmedia.network

import com.squareup.moshi.JsonAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ml.tklabs.elegantmedia.model.SimpleError
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

open class BaseRepository {

    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>, error: String): ApiOutput<T> {

        return withContext(Dispatchers.IO) {

            val result = apiOutput(call, error)
            result
        }
    }

    private suspend fun <T : Any> apiOutput(
        call: suspend () -> Response<T>,
        error: String
    ): ApiOutput<T> {
        return withContext(Dispatchers.IO) {
            try {
                val response = call.invoke()
                if (response.isSuccessful){
                    ApiOutput.Success(response.code(), response.body()!!)
                } else{
                    val out : ApiOutput.GenericError
                    val jsonAdapter : JsonAdapter<SimpleError> = ApiFactory.moshi.adapter(
                        SimpleError::class.java)
                    out = ApiOutput.GenericError(response.code(), jsonAdapter.fromJson(response.body().toString()))
                    out
                }
            }catch (throwable: Throwable){
                when (throwable) {
                    is IOException -> ApiOutput.NetworkError
                    is HttpException -> {
                        val code = throwable.code()
                        ApiOutput.GenericError(code, null)
                    }
                    else -> {
                        ApiOutput.GenericError(null, null)
                    }
                }
            }
        }
    }

}
