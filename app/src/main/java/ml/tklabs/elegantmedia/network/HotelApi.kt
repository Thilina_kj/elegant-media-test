package ml.tklabs.elegantmedia.network

import kotlinx.coroutines.Deferred
import ml.tklabs.elegantmedia.model.HotelListResponse
import retrofit2.Response
import retrofit2.http.GET

interface HotelApi {
    @GET("hotels.json")
    fun requestHotelsAsync()
            : Deferred<Response<HotelListResponse>>
}