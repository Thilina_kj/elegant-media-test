package ml.tklabs.elegantmedia.network

import ml.tklabs.elegantmedia.model.HotelListResponse


class HotelRepo(private val api: HotelApi) : BaseRepository() {

    suspend fun getHotels(): ApiOutput<HotelListResponse>? {
        val response = safeApiCall(
            call = { api.requestHotelsAsync().await() },
            error = "Error Get Data"
        )
        return response
    }

}