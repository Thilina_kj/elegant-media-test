package ml.tklabs.elegantmedia.network

import ml.tklabs.elegantmedia.model.SimpleError


sealed class ApiOutput<out T : Any> {

    data class Success<out T : Any>(val code: Int? = null, val output: T) : ApiOutput<T>()
    data class GenericError(val code: Int? = null, val error: SimpleError? = null): ApiOutput<Nothing>()
    object NetworkError: ApiOutput<Nothing>()

}