package ml.tklabs.elegantmedia.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import ml.tklabs.elegantmedia.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object ApiFactory {
    private val authInterceptor = Interceptor {chain->
        val original = chain.request()
        val authRequest = original.newBuilder()
            .addHeader("app-version", BuildConfig.VERSION_NAME)
            .addHeader("package-id", BuildConfig.APPLICATION_ID)
            .addHeader("os-version", "android")
            .method(original.method(), original.body())
            .build()
        chain.proceed(authRequest)
    }

    private val loggingInterceptor  = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    //OkhttpClient for building http request url
    private val ahClient = if(BuildConfig.DEBUG) {
        OkHttpClient().newBuilder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(authInterceptor)
            .build()
    }else{
        OkHttpClient().newBuilder()
            .addInterceptor(authInterceptor)
            .build()
    }

    private fun retrofit() : Retrofit = Retrofit.Builder()
        .client(ahClient)
        .baseUrl(ApiConstants.baseUrl)
        .addConverterFactory(MoshiConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()

    val moshi : Moshi = Moshi.Builder().build()
    val mainApi : HotelApi = retrofit().create(HotelApi::class.java)
}