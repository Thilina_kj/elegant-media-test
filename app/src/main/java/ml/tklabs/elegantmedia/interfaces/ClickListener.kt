package ml.tklabs.elegantmedia.interfaces

interface ClickListener<T> {
    fun onItemClick(item : T)
}