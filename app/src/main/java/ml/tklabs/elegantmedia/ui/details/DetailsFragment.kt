package ml.tklabs.elegantmedia.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.android.synthetic.main.fragment_details.view.*
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import ml.tklabs.elegantmedia.R
import ml.tklabs.elegantmedia.base.BaseFragment
import ml.tklabs.elegantmedia.ui.home.HomeVM

class DetailsFragment : BaseFragment() {

    private val viewModel: HomeVM by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_details, container, false)
        setViews(view)
        setListeners(view)
        return view
    }

    private fun setViews(view: View) {
        view.tvToolbarTitle.text = getString(R.string.details)
        view.toolbar.apply {
            inflateMenu(R.menu.map_menu)
            menu.findItem(R.id.action_map).setOnMenuItemClickListener {
                findNavController().navigate(R.id.action_detailsFragment_to_mapFragment)
                true
            }
            setNavigationOnClickListener(View.OnClickListener {
                findNavController().navigateUp()
            })
        }
    }

    private fun setListeners(view: View) {
        viewModel.selectedHotel.observe(viewLifecycleOwner, Observer { hotel ->
            hotel.image?.large?.takeIf { it.isNotBlank() }?.also {
                Glide.with(requireContext())
                    .load(it)
                    .placeholder(R.drawable.loading)
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .error(R.drawable.warning)
                    .into(view.ivBanner)
            }
            view.tvTitle.text = hotel.title
            view.tvDesc.text = hotel.description
        })
    }
}