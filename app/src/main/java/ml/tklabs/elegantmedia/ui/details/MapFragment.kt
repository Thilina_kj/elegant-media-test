package ml.tklabs.elegantmedia.ui.details

import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import ml.tklabs.elegantmedia.R
import ml.tklabs.elegantmedia.base.BaseFragment
import ml.tklabs.elegantmedia.model.Hotel
import ml.tklabs.elegantmedia.ui.home.HomeVM

class MapFragment : BaseFragment(), OnMapReadyCallback {

    private val viewModel: HomeVM by activityViewModels()
    private var mMap: GoogleMap? = null
    private var darkThemeSet = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_map, container, false)
        setViews(view)
        setListeners(view)
        return view
    }

    private fun setViews(view: View) {
        view.tvToolbarTitle.text = getString(R.string.map)
        view.toolbar.apply {
            setNavigationOnClickListener(View.OnClickListener {
                findNavController().navigateUp()
            })
        }
    }

    private fun setListeners(view: View) {
        viewModel.selectedHotel.observe(viewLifecycleOwner, Observer { hotel ->
            navigateMap(hotel)
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment?.getMapAsync(this)
    }

    override fun onMapReady(p0: GoogleMap) {
        mMap = p0
        if(!darkThemeSet){
            try {
                p0.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                        requireContext(), R.raw.mapdark))
            } catch (e : Resources.NotFoundException) {
               Log.d("XMap", "Map Theme Failure")
            }
            finally {
                darkThemeSet = true
            }
        }
        viewModel.selectedHotel.value?.also { hotel ->
            navigateMap(hotel)
        }
    }

    private fun navigateMap(hotel: Hotel) {
        val lat = hotel.latitude.takeIf { !it.isNullOrBlank() }?.toDoubleOrNull()
        val lng = hotel.longitude.takeIf { !it.isNullOrBlank() }?.toDoubleOrNull()
        if (lat != null && lng != null) {
            val placeLatLng = LatLng(lat, lng)
            val mo = MarkerOptions()
                .position(placeLatLng)
                .title(hotel.title)
            mMap?.addMarker(mo)
            val cameraPosition = CameraPosition.Builder()
                .target(placeLatLng)
                .zoom(17f)
                .bearing(0f)
                .build();
            mMap?.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        }
    }
}