package ml.tklabs.elegantmedia.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ml.tklabs.elegantmedia.R
import ml.tklabs.elegantmedia.base.BaseFragment

class LoginFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        setViews(view)
        setListeners(view)
        return view
    }

    private fun setViews(view: View) {

    }

    private fun setListeners(view: View) {

    }

}