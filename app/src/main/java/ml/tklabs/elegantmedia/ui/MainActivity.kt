package ml.tklabs.elegantmedia.ui

import android.os.Bundle
import ml.tklabs.elegantmedia.R
import ml.tklabs.elegantmedia.base.BaseActivity

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}