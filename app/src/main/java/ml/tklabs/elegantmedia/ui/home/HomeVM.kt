package ml.tklabs.elegantmedia.ui.home

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.launch
import ml.tklabs.elegantmedia.base.BaseViewModel
import ml.tklabs.elegantmedia.model.Hotel
import ml.tklabs.elegantmedia.model.ResponseStates
import ml.tklabs.elegantmedia.network.ApiFactory
import ml.tklabs.elegantmedia.network.ApiOutput
import ml.tklabs.elegantmedia.network.HotelRepo

class HomeVM : BaseViewModel() {

    var mHotels: MutableLiveData<List<Hotel>> = MutableLiveData()
    var serverResponseStatus: MutableLiveData<Int> = MutableLiveData()
    var selectedHotel: MutableLiveData<Hotel> = MutableLiveData()

    init {
        getHotels()
    }

    /**
     * Get Data calling backend endpoint
     * */
    private fun getHotels() {
        scope.launch {
            val req =
                HotelRepo(ApiFactory.mainApi).getHotels()
            when (req) {
                is ApiOutput.Success -> {
                    req.output.data?.also {
                        mHotels.postValue(it)
                    }
                    serverResponseStatus.postValue(ResponseStates.SUCCESS)
                }
                is ApiOutput.GenericError -> {
                    serverResponseStatus.postValue(ResponseStates.FAILURE)
                }
                is ApiOutput.NetworkError -> {
                    serverResponseStatus.postValue(ResponseStates.NO_INTERNET)
                }
            }
        }
    }

    /**
     * Record user selected hotel for further navigation
     * */
    fun setHotel(hotel: Hotel) {
        selectedHotel.postValue(hotel)
    }
}