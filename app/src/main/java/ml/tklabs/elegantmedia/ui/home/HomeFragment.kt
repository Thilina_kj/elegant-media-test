package ml.tklabs.elegantmedia.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import ml.tklabs.elegantmedia.R
import ml.tklabs.elegantmedia.base.BaseFragment
import ml.tklabs.elegantmedia.interfaces.ClickListener
import ml.tklabs.elegantmedia.model.Hotel
import ml.tklabs.elegantmedia.model.ResponseStates
import ml.tklabs.elegantmedia.ui.home.rv.HotelsAdapter

class HomeFragment : BaseFragment() {

    private lateinit var mAdapter: HotelsAdapter
    private val viewModel: HomeVM by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        setViews(view)
        setListeners(view)
        return view
    }

    private fun setViews(view: View) {
        val listener = object : ClickListener<Hotel> {
            override fun onItemClick(item: Hotel) {
                viewModel.setHotel(item)
                findNavController().navigate(R.id.action_homeFragment_to_detailsFragment)
            }
        }
        mAdapter = HotelsAdapter(
            requireContext(),
            listener
        )
        view.rvItems.layoutManager = LinearLayoutManager(
            context, RecyclerView.VERTICAL, false
        )
        view.rvItems.adapter = mAdapter

        view.tvToolbarTitle.text = getString(R.string.listview)
    }

    private fun setListeners(view: View) {
        view.btnExit.setOnClickListener {

        }
        viewModel.mHotels.observe(viewLifecycleOwner, Observer {
            mAdapter.submitList(it)
        })
        viewModel.serverResponseStatus.observe(viewLifecycleOwner, Observer {
            when (it) {
                ResponseStates.NO_INTERNET -> Toast.makeText(
                    requireContext(), getString(R.string.sww_conn), Toast.LENGTH_SHORT
                ).show()
                ResponseStates.FAILURE -> Toast.makeText(
                    requireContext(), getString(R.string.sww), Toast.LENGTH_SHORT
                ).show()
            }
        })
    }
}