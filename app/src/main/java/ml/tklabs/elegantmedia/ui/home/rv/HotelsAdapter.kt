package ml.tklabs.elegantmedia.ui.home.rv

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.android.synthetic.main.row_hotel.view.*
import ml.tklabs.elegantmedia.R
import ml.tklabs.elegantmedia.interfaces.ClickListener
import ml.tklabs.elegantmedia.model.DiffCallbacks
import ml.tklabs.elegantmedia.model.Hotel

class HotelsAdapter(
    var context: Context,
    private val itemListener: ClickListener<Hotel>? = null
) : ListAdapter<Hotel, RecyclerView.ViewHolder>(DiffCallbacks.Hotel) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.row_hotel, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position) ?: return
        (holder as ItemViewHolder).bind(context, item)
    }

    private inner class ItemViewHolder(private val rootView: View) :
        RecyclerView.ViewHolder(rootView) {

        fun bind(context: Context, item: Hotel) {
            if (item.image != null) {
                Glide.with(context)
                    .load(item.image?.medium)
                    .placeholder(R.drawable.loading)
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .error(R.drawable.warning)
                    .circleCrop()
                    .into(rootView.ivThumb)
            } else {
                rootView.ivThumb.setImageResource(R.drawable.warning)
            }
            rootView.tvTitle.text = item.title
            rootView.tvDesc1.text = item.address

            rootView.setOnClickListener {
                itemListener?.onItemClick(item)
            }
        }
    }

}